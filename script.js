
var log = console.log;
// Activity 18

//#1. STATUS: OK
//#2. STATUS: OK

//#3. STATUS: OK
	let trainer = {

//#4. STATUS: OK
	name: "Ash Ketchum",
	age: 10,
	pokemon: [
		"Pikachu", 
		"Charizard", 
		"Squirtle",
		"Bulbasaur",
	],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]},

};

//#5. STATUS: OK
	trainer.talk = function(){
		log(this.talk);
	};

	log(trainer);

//#6. STATUS: OK
	log(`Result of dot notation:`);
	log(trainer.name);
	log(`Result of bracket notation:`);
	log(trainer["pokemon"]);

//#7. STATUS: OK
	log(`Result of talk method`);
	log(trainer.talk = "Pikachu! I choose you!");
	
//#8. STATUS: OK
	class Pokemon{
  	constructor(name, level){
    	this.name = name;
    	this.level = level;
    	this.health = level * 2;
    	this.attack = level;
  	}
	};

//#9. STATUS: OK
	let pokemon1 = new Pokemon("Pikachu", "12");
	log(pokemon1);

	let pokemon2 = new Pokemon("Geodude", "8");
	log(pokemon2);

	let pokemon3 = new Pokemon("Mewtwo", "100");
	log(pokemon3);

//#10. STATUS: OK

	let statusAndSkill = {
		skillTackle: function (attacker, receiver) {
			log(attacker.name +` tackled `+ receiver.name)
			let newHP = receiver.health -= attacker.attack;
			log(receiver.name + ` 's health is now reduced to `+ newHP)

//#11. STATUS: OK
			//Nested function && defined variable to pass inside the function
			var b = receiver.name;
			let statusFaint = function(){
				return b + ` has fainted.`; 
			}
//#12. STATUS: OK
			if(receiver.health <= 0){
				log(statusFaint());
			}

		},
	 	
	}

//#13. STATUS: OK
	statusAndSkill.skillTackle(pokemon2, pokemon1);
	log(pokemon1);
	statusAndSkill.skillTackle(pokemon3, pokemon2);
	log(pokemon2);


	